﻿namespace SaveTheChange

open System
open System.IO

module Program =
    open CliArguments

    let csvToTransaction cliOpts (data: string) =
        let coli =
            match cliOpts.baseZero with
            | true -> cliOpts.dataColumns
            | false -> 
                {cliOpts.dataColumns with 
                    DateColumn=cliOpts.dataColumns.DateColumn - 1;
                    AmountColumn=cliOpts.dataColumns.AmountColumn - 1
                }

        let pieces = data.Split(",")
        {
            Date=DateTime.Parse pieces.[coli.DateColumn];
            Amount=
                match cliOpts.negativeAmounts with
                | true -> Decimal.Parse pieces.[coli.AmountColumn] * decimal -1
                | false -> Decimal.Parse pieces.[coli.AmountColumn]
        }
    
    let fileDataToTransactions cliOpts lines =
        try
            Some (List.map (fun l -> csvToTransaction cliOpts l) lines)
        with (e) ->
            eprintfn "Failed to convert data: %s" e.Message
            None

    let readDataFile cliOpts =
        let f = Option.get cliOpts.dataFile
        match File.Exists f with
        | true ->
            File.ReadAllLines(f)
            |> Array.toList
            |> if cliOpts.skipHeader then List.skip(1) else List.skip(0)
            |> fileDataToTransactions cliOpts
            |> Option.bind (fun lst ->
                Some (List.where (fun t -> t.Amount > (decimal 0)) lst)) 
        | false ->
            eprintfn "File '%s' does not exist." f
            None

    let getPrevMondayDate (date: DateTime) =
        let offset =
            match date.DayOfWeek with
            | DayOfWeek.Sunday -> -6
            | _ -> (int DayOfWeek.Monday) - (int date.DayOfWeek)

        date.AddDays (float offset)

    let sumTransactions getRoundedChange data =
        List.fold (fun acc trans ->
            match getRoundedChange with
            | true -> acc + (Math.Ceiling(trans.Amount) - trans.Amount)
            | false -> acc + trans.Amount
        ) (decimal 0) data

    let amountByGroup funGroupSelector data = 
        List.groupBy funGroupSelector data
        |> List.map (fun g ->
            let d, groupTrans = (g)
            {
                Date = d
                Amount = sumTransactions false groupTrans
            })
        |> sumTransactions true

    let amountByDay data = 
        amountByGroup (fun t -> t.Date) data
        
    let amountByTransaction data = 
        sumTransactions true data

    let amountByWeek data = 
        amountByGroup (fun t -> getPrevMondayDate t.Date) data


    let printAmount amtTypeMsg (amount: decimal) =
        printfn "Amount by %s: $%.2f" amtTypeMsg (Math.Ceiling amount)

    [<EntryPoint>]
    let main argv =
        let cliOpts = parseCommandLine argv
        match cliOpts.dataFile with
        | Some s -> ()
        | None -> 
            eprintfn "Data file argument missing."
            Environment.Exit 1

        let data = readDataFile cliOpts
        match data with
        | Some d -> 
            printAmount "Transaction" (amountByTransaction d)
            printAmount "Day" (amountByDay d)
            printAmount "Week" (amountByWeek d)
            0
        | None -> 1
