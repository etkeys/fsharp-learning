namespace SaveTheChange

open System

[<AutoOpen>]
module DomainTypes = 
    let progversion = "1.1-a"

    type Transaction = { Date: DateTime; Amount: decimal }


    type CliOptionBaseZero = bool
    type CliOptionDataColumnMapping = { DateColumn: int; AmountColumn: int; }
    type CliOptionDataFile = string option
    type CliOptionNegativeAmounts = bool
    type CliOptionPrintHelp = bool
    type CliOptionPrintVersion = bool
    type CliOptionSkipHeader = bool

    type CliOptions = {
        baseZero: CliOptionBaseZero;
        dataColumns: CliOptionDataColumnMapping;
        dataFile: CliOptionDataFile;
        negativeAmounts: CliOptionNegativeAmounts;
        printHelp: CliOptionPrintHelp;
        printVersion: CliOptionPrintVersion;
        skipHeader: CliOptionSkipHeader;
    }