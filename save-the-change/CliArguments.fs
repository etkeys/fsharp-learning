namespace SaveTheChange

open System

module CliArguments = 

    let printVersionMessage quitWhenDone = 
        printfn "Save the change (version %s)" progversion
        match quitWhenDone with
        | true -> Environment.Exit 2
        | false -> ()

    let printHelpMessage  =
        printVersionMessage false
        let msg = "
usage: [OPTIONS] file
file
    The data file to read from. CSV format.

-0, --base-zero
    Treat indexes in column list as zero based indexing.

-c, --columns DATE_INDEX;AMOUNT_INDEX
    Specify a list of column indexes that will be the date and amount 
    columns (in that order) seperated by a comma (,).

-H, --no-skip-header
    The data file does not contain a header on the first line, so the
    first line should be processed as a ordinary data line.

-N, --negative-amounts
    Assume amounts in the data file are negative values.

-h, --help
    Display the help message.

--version
    Display the version information of the program."
        printfn "%s" msg
        Environment.Exit 2

    let rec parseCommandLineImpl optsAcc args =
        match args with
        // empty list means we're done.
        | [] -> optsAcc

        // Flag for zero based indexing
        | "-0"::xs | "--base-zero"::xs ->
            let newOptsAcc = {optsAcc with baseZero=not optsAcc.baseZero}
            parseCommandLineImpl newOptsAcc xs

        // Get the specified column indexes
        | "-c"::xs | "--columns"::xs ->
            let x, xxs = xs.Head, xs.Tail
            let newColumns = (fun (s:string) ->
                let parts = s.Split(",")
                {
                    DateColumn = int parts.[0]
                    AmountColumn = int parts.[1]
                }) 
            let newOptsAcc = {optsAcc with dataColumns = (newColumns x)}
            parseCommandLineImpl newOptsAcc xxs
        
        // Flag for header inclusion
        | "-H"::xs | "--no-skip-header"::xs ->
            let newOptsAcc = {optsAcc with skipHeader=not optsAcc.skipHeader}
            parseCommandLineImpl newOptsAcc xs
        
        // Flag for inverting amount values (e.g. negative becomes positive)
        | "-N"::xs | "--negative-amounts"::xs ->
            let newOptsAcc = 
                {optsAcc with negativeAmounts=not optsAcc.negativeAmounts}
            parseCommandLineImpl newOptsAcc xs
        
        // Print usage and help message
        | "-h"::xs | "--help"::xs ->
            let newOptsAcc = {optsAcc with printHelp=not optsAcc.printHelp}
            parseCommandLineImpl newOptsAcc xs

        // Print version information
        | "--version"::xs ->
            let newOptsAcc = {optsAcc with printVersion=true}
            parseCommandLineImpl newOptsAcc xs

        // Unflagged options, should be the data file
        | a::xs ->
            let newOptsAcc = {optsAcc with dataFile=Some a}
            parseCommandLineImpl newOptsAcc xs

    let parseCommandLine args = 
        let defaultOpts = {
            baseZero=false;
            dataColumns={DateColumn=1;AmountColumn=6}
            dataFile=None;
            negativeAmounts=false;
            printHelp=false;
            printVersion=false;
            skipHeader=true;
        }

        let finalOpts = 
            Array.toList args 
            |> List.skip(1) 
            |> parseCommandLineImpl defaultOpts

        match finalOpts with
        | h when finalOpts.printHelp ->
            printHelpMessage 
        | v when finalOpts.printVersion ->
            printVersionMessage true
        | x -> ()
        
        finalOpts