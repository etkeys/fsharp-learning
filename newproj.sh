#!/bin/bash

projtype="${2:-console}"
dotnet new "$projtype" -lang 'F#' -o "$1"
