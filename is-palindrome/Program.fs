﻿open System

let ignoredChars = [",";"'";".";"?";" "]

let isValidWord word = not(String.IsNullOrWhiteSpace(word))

let cleanWord (word:string) =
    let w = word.ToUpper()
    List.fold 
        (fun (acc:string) c -> acc.Replace(c, "")) w ignoredChars
    
let isPalindrome (word:string) = 
    let wordrev = word.ToCharArray() |> Array.rev |> String |> string
    eprintfn "Original word: %s" word
    eprintfn "Reverse word: %s" wordrev
    word = wordrev

let testWord inp =
    cleanWord inp
    |> isPalindrome
    |> function
        | true -> "is a palindrome."
        | false -> "is not a palindrome."
    |> printfn "\"%s\" %s" inp

let getUserWord = fun _ -> 
    printf "Input to test (leave blank to quit): "
    Console.ReadLine()

[<EntryPoint>]
let main argv =
    Seq.initInfinite getUserWord
    |> Seq.takeWhile isValidWord
    |> Seq.iter testWord
    0
