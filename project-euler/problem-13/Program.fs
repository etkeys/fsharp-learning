﻿open System

type PartitionedNumber = list<string>

let partition (inp:string) =
    inp.ToCharArray()
    |> (Array.rev >> Array.toList)
    |> List.chunkBySize 4   // 4 digit numbers
    |> List.map (List.toArray >> Array.rev >> String.Concat)

let appendCarry indx (carry:PartitionedNumber) inp = 
    match indx with
    | 0 -> inp 
    | _ when carry.Length <= indx -> inp
    | _ -> Seq.append inp [carry.[indx]]

let add2Strings x y =
    [x;y]
    |> List.sumBy (fun s -> s |> int)
    |> string

let sumSinglePartition indx carry inp =
    Seq.map (fun (p:PartitionedNumber) -> p.[indx]) inp
    |> appendCarry indx carry
    |> Seq.fold add2Strings "0"
    |> partition
    |> function
        | x when x.IsEmpty -> x
        | y -> carry.[..(indx - 1)] @ y;

let sumAllPartitions (inp:seq<PartitionedNumber>) =
    Seq.map (fun (i:PartitionedNumber) -> i.Length) inp
    |> (Seq.max >> fun m -> [0..m-1]) // List of partition indexes to process
    |> List.fold (fun acc i ->
        sumSinglePartition i acc inp
    ) PartitionedNumber.Empty
    |> List.rev

[<EntryPoint>]
let main argv =
    System.IO.File.ReadAllLines("data.dat")
    |> Array.toSeq
    |> Seq.map partition
    |> sumAllPartitions
    |> fun ls -> String.Join("", ls).[..10]
    |> printfn "The first 10 digits are: %s"
    0
