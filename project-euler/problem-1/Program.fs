﻿
open System

[<EntryPoint>]
let main argv =
    {3 .. 1 .. 999}
    |> Seq.filter (fun i -> i % 3 = 0 || i % 5 = 0)
    |> Seq.sum
    |> printfn "Sum is: %i"
    0
